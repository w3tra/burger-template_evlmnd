import React, {Component, Fragment} from 'react';
import CheckOutSummary from "../../components/Order/CheckOutSummary";
import {Route} from 'react-router-dom';
import ContactData from '../../containers/Checkout/ContactData';


class Checkout extends Component {

    state = {
        ingredients: {
            salad: 1,
            bacon: 1,
            meat: 1,
            cheese: 1
        },
        price: 0
    };

    componentDidMount() {   /////
        const query = new URLSearchParams(this.props.location.search);

        const ingredients = {};
        let price = 0;

        for (let param of query.entries()) {
            if (param[0] === 'price') {
                price = parseInt(param[1]);
            } else {
                ingredients[param[0]] = parseInt(param[1], 10);
            }
            // ingredients[param[0]] = parseInt((param[1]));
        }
        // console.log(price);

        this.setState({ingredients, price});  //// из той же истории (BurgerBuilder.js 84)
    }

    checkoutCancelled = () => {
        this.props.history.goBack();
    };

    checkoutContinued = () => {
        this.props.history.replace('/checkout/contact-data');
    };


    render() {
        return (
            <Fragment>
                <CheckOutSummary
                    ingredients={this.state.ingredients}
                    checkoutCancelled={this.checkoutCancelled}
                    checkoutContinued={this.checkoutContinued}
                />
                <Route
                    path={this.props.match.path + '/contact-data'}
                    render={(props) => (
                        <ContactData
                            ingredients={this.state.ingredients}
                            price={this.state.price}
                            {...props}
                        />
                    )}
                />
            </Fragment>
        );
    }
}

export default Checkout;