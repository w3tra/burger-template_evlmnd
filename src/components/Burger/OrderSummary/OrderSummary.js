import React from 'react';
import Button from '../../UI/Button/Button';

const OrderSummary = props => {

    const IngrSummary = Object.keys(props.ingredients).map(igKey => (
        <li key={igKey}>
            <span>{igKey}</span>: {props.ingredients[igKey]}
        </li>
    ));

    return (
        <div>
            <h3>Order</h3>
            <p>your burger</p>
            <ul>
                {IngrSummary}
            </ul>
            <p>Total price: {props.price}</p>
            <p>continue?</p>
            <Button btnType="Danger"
                    onClick={props.purchaseCancel}
            >
                CANCEL
            </Button>
            <Button
                btnType="Success"
                onClick={props.purchaseContinue}
            >
                CONTINUE
            </Button>
        </div>
    )
};

export default OrderSummary;