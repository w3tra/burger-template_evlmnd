import React from 'react';
import Burger from "../Burger/Burger";
import Button from '../UI/Button/Button';
// import './CheckOutSummary.css';

const CheckOutSummary = (props) => {
    return (
        <div>
            <div className="CheckoutSummary">
                <h1>Hi</h1>
                <div>
                    <Burger ingredients={props.ingredients}/>
                </div>
                <Button
                btnType="Danger"
                onClick={props.checkoutCancelled}>Cancel</Button>
                <Button
                    btnType="Success"
                    onClick={props.checkoutContinued}>Continue</Button>
            </div>
        </div>
    );
};

export default CheckOutSummary;